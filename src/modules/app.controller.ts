import { Controller, Get, Req, Res } from '@nestjs/common';

@Controller()
export class AppController {
    
    @Get()
    root(@Res() resp) {
        resp.render('index.html', {world: 'World'});
    }
}