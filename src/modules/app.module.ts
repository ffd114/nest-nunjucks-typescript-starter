import { Module, NestModule, MiddlewaresConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';

@Module({
    controllers: [AppController],
})
export class ApplicationModule { }