import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './modules/app.module';
import * as express from 'express';
import * as nunjucks from 'nunjucks';
import * as session from 'express-session';

async function bootstrap() {

	var server = express();

	nunjucks.configure('./src/views', {
		express: server
	  });
	
	server.use(session({
		secret: 'myfirstnestjs',
	}));

	const app = await NestFactory.create(ApplicationModule, server);
	await app.listen(3000);
}
bootstrap();
